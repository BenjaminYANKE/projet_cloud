#!/usr/bin/env bash

sleep 60

{
	sudo apt -y update && apt -y upgrade
	sudo apt -y install appache2
	sudo systemctl enable apache2 && systemctl start apache2
	sudo apt -y install php8.2 php8.2-cli php8.2-common php8.2-imap php8.2-redis php8.2-snmp php8.2-xml php8.2-mysqli php8.2-zip php8.2-mbstring php8.2-curl php8.2-gd libapache2-mod-php
	sudo apt -y install git
} > /root/apt.log 2> /root/apt.err.log

cd /var/www/html

sudo wget https://ftp.drupal.org/files/projects/drupal-10.1.2.zip 
sudo unzip drupal-10.1.2.zip 
mv drupal-10.1.2/ drupal/ 
rm drupal-10.1.2.zip
chown -R www-data:www-data drupal/ 
#find . -type d -exec chmod 755 {} \; 
#find . -type f -exec chmod 644 {} \;

cd ~/
git clone https://gitlab.com/thomas_hru/drupal-config-files.git

cd /etc/apache2/sites-available/ 
cp ~/drupal.conf .

sudo a2dissite 000-default.conf
#sudo a2enmod rewrite
sudo a2ensite drupal.conf

sudo systemctl stop apache2 && sudo systemctl start apache2

reboot
