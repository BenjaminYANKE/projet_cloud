# Instance Drupal seul

## Composants ToFu

### instances.tf
Dans l'objectif de déployer une instance Drupal, ce fichier permet :
    - D'initialiser une instance Scaleway
    - Installer et configurer Drupal

Pour cela, le script init_drupal.sh réalise ce simple objectif et pourra être réutiliser pour initialiser un nombre d'instance souhaité.

Afin de déployer une instance :

    1. Définir le préfixe des composants au sein de `teamname.txt`

    2. `make init` afin d'initialiser le projet ToFu, si jamais cela n'a pas été fait au préalable

    3. `make plan` afin de vérifier si les composants et le projet ont été correctement définis avant l'exécution

    4. `make apply` afin d'exécuter le projet et mettre en place l'instance


# Architecture

## Composants ToFu

### lb.tf
Le load-balancer permet de rediriger publiquement les clients vers deux serveurs web. Ces serveurs sont les instances Drupal configurés ci-dessus.
Il ne s'ouvre que que le port 80 pour que les navigateurs internet se dirigent automatiquement vers le service web adéquat.

Côté backend, les instances Drupal et la base de données configurées sont assignés à un VPC, ce qui demande une assignation statique des adresses IP privées (Ici, entre 192.168.40.100/24 et 192.168.40.200/24).

### vpc.tf
Le réseau privé virtuel permet la communication entre chaque élément, par exemple pour permettre aux instances drupal d'interroger la base de données.

Ce vpc détient une "Public Gateway" faisant office de bastion. Grâce à lui, seuls les personnes acrédités, grâce aux clés SSH configurés sur le site de scaleway, auront la possibilité de se connecter à distance sur les serveurs. Il est nécessaire de passer par le bastion pour se connecter sur une instance. 

### locals.tf & teamname.txt
Afin de faciliter la nomanclature des éléments du projet et ne pas avoir une liste d'élément facilement confondable, locals.tf est un fichier qui se comporte comme une variable.

Un script shell récupère un préfixe noté dans teamname.txt et le stocke dans la variable ${locals.team}. De cette façon, au lieu de changer le préfixe du nom chaque élément, il suffira de le changer qu'une fois, dans ce fichier.

### Makefile
Le Makefile est un outil nous permettant de lancer des scripts avec une simple commande.
Grâce à lui il est possible d'initialiser un projet ToFu, de le vérifier, de le construire, de le détruire. C'est aussi ici que le préfixe stocké dans teamname.txt sera appliqué dans locals.tf.

## Scripts d'initialisations
Le projet possède deux scripts d'initialisation : init_drupal (vu dans la première partie ci-dessus) et init_db.
Comme leur nom l'indique, ce sont des scripts pré-rédigés qui seront exécutés au sein des instances assignés lors de leur création.

Si les serveurs Drupal où la base de données doivent respecter un processus de création détaillée, touchant par exemple leur configuration, alors il sera facile de modifier cela dans ces scripts.





# Guide d'Installation - Hébergement de Drupal dans le Cloud

Ce guide détaille les étapes nécessaires pour déployer et configurer un environnement d'hébergement pour Drupal dans le cloud, en utilisant Tofu comme gestionnaire de configuration et MariaDB avec Apache2 comme serveur de base de données et serveur web.

## Prérequis

Avant de commencer le déploiement, assurez-vous d'avoir les éléments suivants :

1 - Un compte avec les droits d'administration sur votre service cloud préféré 
(par exemple, AWS, Azure, Google Cloud,ScaleWay etc.).
2 - Accès SSH à votre instance cloud.
3 - Un domaine configuré pour votre application Drupal (facultatif pour le développement local).

## Installation

1 - Cloner le Projet

- git clone https://github.com/votre-utilisateur/projet-drupal-cloud.git

2 - Configuration de Tofu

Assurez-vous d'avoir configuré Tofu avec les informations d'accès à votre service cloud. Vous pouvez consulter la documentation officielle de Tofu pour plus de détails.

3 - Exécution du Script d'Installation
Une fois que vous êtes connecté à votre instance cloud via SSH, exécutez le script d'installation fourni :

- chmod +x init_drupal.sh
- ./init_drupal.sh

Ce script met à jour le système, installe et configure Apache2, MariaDB, PHP et d'autres dépendances nécessaires, télécharge et configure Drupal, et configure un hôte virtuel Apache2 pour servir votre site Drupal.

4 - Configuration de MariaDB
Une fois l'installation terminée, connectez-vous à votre serveur MariaDB et configurez la base de données pour Drupal :

- sudo mysql -u root -p

Puis, dans le shell MySQL, exécutez les commandes suivantes :

- CREATE USER 'drupal'@'localhost' IDENTIFIED BY 'YourStrongPasswordHere'; 
CREATE DATABASE drupal; 
GRANT ALL PRIVILEGES ON drupal.* TO 'drupal'@'localhost'; 
FLUSH PRIVILEGES;
EXIT;

5 - Configuration du Domaine (Optionnel)

Si vous avez un domaine configuré pour votre application Drupal, assurez-vous de configurer les enregistrements DNS pour pointer vers l'adresse IP de votre instance cloud.

## Utilisation

Une fois l'installation et la configuration terminées, vous pouvez accéder à votre site Drupal via le navigateur en accédant à l'adresse IP de votre instance cloud ou via votre domaine configuré.

Assurez-vous de personnaliser et de sécuriser davantage votre installation Drupal selon vos besoins spécifiques.
