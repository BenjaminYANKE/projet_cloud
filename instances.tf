#Première instance

resource "scaleway_instance_server" "srv1" {
    name  = "${local.team}-srv1"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = [ "${local.team}", "web2", "drupal" ]
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "web"
       cloud-init = file("${path.module}/init_drupal.sh")
    }
    enable_ipv6 = false
    private_network {
       pn_id = scaleway_vpc_private_network.myvpc.id
    }
}

# Seconde instance

resource "scaleway_instance_server" "srv2" {
    name  = "${local.team}-srv2"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = [ "${local.team}", "web1", "drupal" ]
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "web"
       cloud-init = file("${path.module}/init_drupal.sh")
    }
    enable_ipv6 = false
    private_network {
       pn_id = scaleway_vpc_private_network.myvpc.id
    }
}

# Base de données

resource "scaleway_instance_server" "db" {
    name  = "${local.team}-db"
    type  = "PRO2-XXS"
    image = "debian_bookworm"
    tags = [ "${local.team}", "db" ]
    root_volume {
      size_in_gb = 10
    }
    user_data = {
       role       = "db"
       cloud-init = file("${path.module}/init_db.sh")
    }
    enable_ipv6 = false
    private_network {
       pn_id = scaleway_vpc_private_network.myvpc.id
    }
}

# Données en sortie

output "srv1_private_ip" {
  value = "${scaleway_instance_server.srv1.private_ip}"
}

output "srv2_private_ip" {
  value = "${scaleway_instance_server.srv2.private_ip}"
}

output "db_private_ip" {
  value = "${scaleway_instance_server.db.private_ip}"
}
