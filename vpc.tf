resource scaleway_vpc_private_network "myvpc" {
  name = "${local.team}-vpc"
}

resource scaleway_vpc_public_gateway_ip "pgw_ip" {
}

resource scaleway_vpc_public_gateway "pgw" {
  name = "${local.team}-pgw"
  type = "VPC-GW-S"
  bastion_enabled = true
  ip_id = scaleway_vpc_public_gateway_ip.pgw_ip.id
}

resource scaleway_vpc_public_gateway_dhcp "dhcp" {
  subnet = "192.168.42.0/24"
  dns_local_name = scaleway_vpc_private_network.myvpc.name
  pool_low = "192.168.42.100"
  pool_high  = "192.168.42.200"
}

resource scaleway_vpc_public_gateway_dhcp_reservation frontsrv1 {
    gateway_network_id = scaleway_vpc_gateway_network.mygw.id
    mac_address = scaleway_instance_server.srv1.private_network[0].mac_address

    ip_address = "192.168.42.101"
}

resource scaleway_vpc_public_gateway_dhcp_reservation frontsrv2 {
    gateway_network_id = scaleway_vpc_gateway_network.mygw.id
    mac_address = scaleway_instance_server.srv2.private_network[0].mac_address

    ip_address = "192.168.42.102"
}

resource scaleway_vpc_public_gateway_dhcp_reservation frontdb {
    gateway_network_id = scaleway_vpc_gateway_network.mygw.id
    mac_address = scaleway_instance_server.db.private_network[0].mac_address

    ip_address = "192.168.42.103"
}

resource scaleway_vpc_gateway_network "mygw" {
  gateway_id          = scaleway_vpc_public_gateway.pgw.id
  private_network_id  = scaleway_vpc_private_network.myvpc.id
  dhcp_id             = scaleway_vpc_public_gateway_dhcp.dhcp.id
  enable_dhcp         = true
}


output "bastion_ip" {
  value = "${scaleway_vpc_public_gateway_ip.pgw_ip.address}"
}

