#!/usr/bin/env bash

sleep 60

{
	apt -y update && apt -y upgrade
	apt -y install mariadb-server
} > /root/apt.log 2> /root/apt.err.log

systemctl start mariadb
systemctl enable mariadb

mysql_secure_installation<<EOT
teambio-root
Y
N
Y
Y
Y
EOT

systemctl restart mariadb

mysql -u root -pteambio-root<<EOT
CREATE USER 'drupal'@'%' IDENTIFIED BY 'drupal-pwd';
CREATE DATABASE drupal;
GRANT ALL PRIVILEGES ON drupal.* TO 'drupal'@'%';
FLUSH PRIVILEGES;
EXIT;
EOT

reboot
